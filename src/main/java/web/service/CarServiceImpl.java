package web.service;

import web.model.Car;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CarServiceImpl implements CarService {

    private List<Car> cars = List.of(
            new Car("Toyota", "Camry", "Silver"),
            new Car("Kia", "Rio", "White"),
            new Car("Peugeot", "308", "Blue"),
            new Car("Peugeot", "406", "Red")
    );


    @Override
    public List<Car> getAllCars() {
        return cars;
    }
}
