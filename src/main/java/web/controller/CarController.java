package web.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import web.service.CarService;

@Controller
@RequestMapping("/cars")
public class CarController {

    private final CarService carService;

    public CarController(CarService carService) {
        this.carService = carService;
    }

    @RequestMapping(method = RequestMethod.GET)
    public String printCars(ModelMap modelMap, @RequestParam(required = false) String locale) {
        String message;
        if ("ru".equals(locale)) {
            message = "МАШИНЫ";
        } else {
            message = "CARS";
        }
        modelMap.addAttribute("title", message);
        modelMap.addAttribute("cars", carService.getAllCars());
        return "cars";
    }


}
