<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Cars</title>
</head>
<body>
<h1>
    <c:out value="${title}"/>
</h1>
<table>
    <thead>
    <tr>
        <th>
            Brand
        </th>
        <th>
            Model
        </th>
        <th>
            Color
        </th>
    </tr>
    </thead>
    <c:forEach var="car" items="${cars}">
        <tr>
            <td>
                    ${car.brand}
            </td>
            <td>
                    ${car.model}
            </td>
            <td>
                    ${car.color}
            </td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
